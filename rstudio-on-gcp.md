# RStudio on Google Cloud

*Objectives*
1. [Install R](#install-r)
1. [Set up RStudio Server on a VM within GCP](#install-rstudio-server)

We will be using the open-source RStudio Server *integrated development
environment* (IDE). IDEs are designed to maximize programming productivity by
encompassing useful features such as package/library documentation or a
*graphical user interface* (GUI) for a version control system into a single
program.

## Prerequisites
1. [a VM on GCP](./vm.md)
    - a [firewall rule](./vm.md#define-a-firewall-rule) for port 8787
    - [Anaconda](./vm.md#install-anaconda)
    - [Git](./vm.md#install-git)

## Install R
We first need to install the R programming language to the VM.

1. To ensure the integrity of files, add an authentication key to the system: `sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E084DAB9`
1. Add a new website to the sources list to indicate an additional place to look for packages.
    - `sudo nano /etc/apt/sources.list`
        - `sudo`: execute command as a superuser (root) for additional privileges
        - `nano`: text editor
        - `/etc/apt/sources.list`: file to open and edit
    - add this line to the end of the file which is specific for : `deb https://cloud.r-project.org/bin/linux/ubuntu xenial-cran35/`
    <center><img src="resources/img/rstudio-r-cloud.png" width="600"></center>
1. Install the required packages and dependencies in the SSH browser window:
    - `sudo apt-get update`
        - `sudo`: execute command as a superuser (root) for additional privileges
        - `apt-get`: tool for handling packages using the APT library
        - `update`: resynchronize the package index files from their sources
    - `sudo apt-get install -y r-base`
        - `sudo`: execute command as a superuser (root) for additional privileges
        - `apt-get`: tool for handling packages using the APT library
        - `install`: install mode
        - `-y`: answer yes for all prompts
        - `r-base`: install R
1. Check that `R` installed correctly:
    <center><img src="resources/img/rstudio-R.png" width="500"></center>


Reference:
- [Updating R on Ubuntu](https://www.r-bloggers.com/updating-r-on-ubuntu/)

### Install RStudio Server
Now that we have R installed, we are ready to install RStudio Server.

1. To install the latest RStudio Server version for 64-bit Debian Linux, go to the [RStudio website](https://www.rstudio.com/products/rstudio/download-server/).
    - It should look something like this:
        - `sudo apt-get install gdebi-core`
            - gdebi is a tool to install deb files.
            <center><img src="resources/img/rstudio-install-gdebi-core.png" width="600"></center>

        - `wget https://download2.rstudio.org/rstudio-server-1.1.463-amd64.deb`
        <center><img src="resources/img/rstudio-download-RStudio.png" width="600"></center>

        - `sudo gdebi rstudio-server-1.1.463-amd64.deb`
        <center><img src="resources/img/rstudio-install-gdebi-core.png" width="600"></center>
1. The RStudio Server should be running now:
    <center><img src="resources/img/rstudio-gdebi-rstudio-server.png" height="500"></center>
1. We need to create a user account to login into RStudio: `sudo adduser [USER_NAME]`
    - Replace `[USER_NAME]` with the new username. You will be prompted to enter a password for the new user.
     <center><img src="resources/img/rstudio-adduser.png" width="500"></center>
1. Find the VM's external IP.
    <center><img src="resources/img/rstudio-external-ip.png" width="550"></center>
1. Open up a web browser and navigate to `external_ip:8787`. Enter the username and password that for the user that you just created.
    <center><img src="resources/img/rstudio-sign-in.png" width="500"></center>

References:
- [Ubuntu Packages for R](https://cran.r-project.org/bin/linux/ubuntu/README.html)

### Setting Up a Conda Environment

Note: This installs the R library `feather` which is required to read the binary data files included in our [data challenge](https://gitlab.com/labsysmed/data-challenge)

1. To update the conda version: `conda update -n base conda`
1. To create a new conda environment: `conda create --name data-challenge r-feather`
    - `conda`: command line tool for Anaconda
    - `create`: create a new environment
    - `--name`: name of the conda environment (`data-challenge`)
    - `r-feather`: install the r-feather package
1. To activate the environment in terminal: `source activate data-challenge`
1. To deactivate the environment in terminal: `source deactivate`
1. To include the conda environment's libraries to RStudio's `.libPaths()`, copy the following script to a file and make it an executable. To use this script, we would type `./script.sh data-challenge`.


```
# First argument is a name of the conda environment that needs to be activated
echo Target environment is: $1

# Find path to the environment (second column of output of command)
new_env_path=`conda info --env | grep -E "^$1\s+" | awk '{print $2}'`

# In case if the environment is active in conda - read path in third column
if [ "$new_env_path" = "*" ]; then
    echo "Env is active!"
    new_env_path=`conda info --env | grep -E "^$1\s+" | awk '{print $3}'`
else
    echo "Env is not active"  
fi;

echo "New path = $new_env_path"

# sudo is required to edit configuraion file
sudo sed -i "s|rsession-which-r=.*|rsession-which-r=$new_env_path/bin/R|" /etc/rstudio/rserver.conf

# ... and to restart service
sudo rstudio-server restart
```
source: https://github.com/ContinuumIO/anaconda-issues/issues/9423
