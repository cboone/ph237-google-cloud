# PH237: Google Cloud

## Overview
- [What is a cloud computing environment?](./cloud-computing.md)
- [Software Tools Overview](./software-tools-overview.md)
    - Linux
    - Version control using [Git](./git.md)
    - Reproducibility with Anaconda
    - Development environment such as RStudio, Jupyter notebooks

## Tutorials
- Setting up:
    - On Google Cloud Platform:
        - [Setup](./gcp-setup.md): redeeming GCP credits, creating a project
        - [Create a VM](./vm.md)
            - Install Git, Anaconda
        - Development Environment
            - [RStudio](./rstudio-on-gcp.md)
            - [Jupyter Notebooks](./jupyter_notebook.md)
- Try out your new skills by completing our [data challenge](https://gitlab.com/labsysmed/data-challenge)!
    - [Tutorials on Data Tidying and Transformation in Python and R](https://github.com/diyadas/tutorials)


## What's next?
The tech space moves very quickly with a ton of jargon. It can
be overwhelming but reading documentation is very helpful. Remember, often
technology is not the problem rather a tool to build solutions. Focus on finding
good problems first!

## Appendix
- [Collaboration on GCP](./gcp-collaboration.md)
